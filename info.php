<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kyan Woods - More Info</title>
        <meta name="description" content="This is my personal website." />
        <link rel="stylesheet" href="style.css" /> 
        <a href="about.php">About Me</a>
        <a href="gamedatabase.php">List of Games Submitted</a>
        <a href="games.php">Favorite Games</a>
        <a href="index.php">Game Submission</a>
        <a href="info.php">More Information</a>
        <a href="sonainfo.php">Info on my Fursonas</a>
    </head>

<body>
    <div class="divclass">
        <p class="greenText">I am part of a robotics team which participates in FIRST Robotics. I participate in a bit of robotic building.</p>
        <img src="https://th.bing.com/th/id/OIP.H6PGlAI6h5-lui64485NYwHaFy?w=237&h=186&c=7&r=0&o=5&dpr=1.5&pid=1.7" alt="FIRST Robotics Competition Logo">
        <p class="greenText">I am a part of the furry fandom and have been for about a year or so. I have about 13-14 fursonas as of creating this line of code. 
            They are a wide variety of species as well. They range from cats to protogens. <a href="sonainfo.php">More info here.</a> I plan on making a 
            ton more fursonas during my time as a furry. I want to get a fursuit made of my main protogen sona in the future when I save up enough money. 
            I would really love to be able to get a job which pays a lot per hour since that would mean that I get more money per paycheck which then means 
            I can get more things to show off my love for certain things and whatnot. I would love to have a mechanical helmet which represents a protogen 
            due to the whole electrical component of the lights and electronics on the inside. The coding portion of making all of the LED lights light up 
            in certain patterns also interests me. I plan on eventually learning how to code things similar to that for others so I can help them with what 
            they would need done.</p>
        <p class="greenText">I am the type of person that puts other people above myself at times because of the things that I have gone through. I tend to try to make other 
            people happy when I should focus on myself. I like making others feel better than I do because I do not want others to feel the same way as me. I 
            like to keep the atmosphere around me as happy as I can make it. It is always better to have a happy aura than a depressing one. Even though I have 
            a very pessimistic outlook on life, I tend to have an optimistic outlook on what I believe other people will do with their lives.</p>
    </div>
</body>
</html>