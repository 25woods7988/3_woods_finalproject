<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kyan Woods - Game Submission</title>
        <meta name="description" content="This is my personal website." />
        <link rel="stylesheet" href="style.css" /> 
        <a href="about.php">About Me</a>
        <a href="gamedatabase.php">List of Games Submitted</a>
        <a href="games.php">Favorite Games</a>
        <a href="index.php">Game Submission</a>
        <a href="info.php">More Information</a>
        <a href="sonainfo.php">Info on my Fursonas</a>
    </head>

<body>
    <div class="divclass">
        <h1>Kyan Woods</h1>
        <p class="greenText">Insert your favorite game and the platform you play it on! Don't worry, I am not going to judge you. 
            I have plenty of games of my own that I don't like to share.</p>
    </div>
    <div> 
        <form action = "submit.php" method = "POST">
            <label class="greenText" for = "gamename">Game Name</label><br>
            <input type = "text" id = "gamename" name = "gamename"><br>
            <label class="greenText" for = "platform">Platform</label><br>
            <input type = "text" id = "platform" name = "platform">
            <input type = "submit" value = "Submit">
        </form>
    </div>
</body>
</html>