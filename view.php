<div>
<table>
<tr>
    <th>Game Name</th>
    <th>Platform</th>
    <th>ID</th>
</tr>
<?php
//Description: Take our form data and display it

require('config.php');

$mysqli = mysqli_connect($host, $user, $password, $db);

if (!$mysqli) {
    echo 'Connection Failed.<br>';
    echo 'Error Message: ' . mysqli_connect_error();
    die();
} 

$query = "select * from gaming";

if ($result_set = $mysqli->query($query)) {
    while($row = $result_set->fetch_array(MYSQLI_ASSOC)) {
       echo "
        <tr>
            <td>" . $row['gamename'] . "</td>
            <td>" . $row['platform'] . "</td>
            <td>" . $row['id'] . "</td>
        </tr>";
    }
}


?>
</table> 
</div>   