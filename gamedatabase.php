<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kyan Woods - Games Submitted</title>
        <meta name="description" content="This is my personal website." />
        <link rel="stylesheet" href="style.css" /> 
        <a href="about.php">About Me</a>
        <a href="gamedatabase.php">List of Games Submitted</a>
        <a href="games.php">Favorite Games</a>
        <a href="index.php">Game Submission</a>
        <a href="info.php">More Information</a>
        <a href="sonainfo.php">Info on my Fursonas</a>
    </head>

<body>
    <div>
    <?php require('view.php'); ?>
    </div>
</body>
</html>