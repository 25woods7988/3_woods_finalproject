<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kyan Woods - Favorite Games</title>
        <meta name="description" content="This is my personal website." />
        <link rel="stylesheet" href="style.css" /> 
        <a href="about.php">About Me</a>
        <a href="gamedatabase.php">List of Games Submitted</a>
        <a href="games.php">Favorite Games</a>
        <a href="index.php">Game Submission</a>
        <a href="info.php">More Information</a>
        <a href="sonainfo.php">Info on my Fursonas</a>
    </head>

<body>
    <div class="divclass">
        <p class="greenText">I have many favorite games. Some of them include SUPERHOT, Cult of the Lamb, Terraria, DOOM: Eternal, DOOM (1993), Pong, Neon White, 
            and many others.</p>
        <p class="greenText"> Many games that I love are on switch which is my main platform for gaming. I enjoy many different types of genres and I am usually open to 
            trying new genres. I tend to usually start many games and then either finish them or move on when I get bored. I have a Nintendo Switch, a 
            Nintendo 3DS XL (one of the models from years ago), a mini Space Invaders arcade machine type game, a handheld Oregon Trail game, and 
            a Pac-Man handheld game. I usually play games that have some type of sandbox style or some games with an 8-bit/pixel-art style since 
            I really enjoy those types of games. The 8-bit/pixel-art style has always been my favorite since it makes me think of classic video games 
            and just the simplicity of the art due to the machine limitations of the time. I like the sandbox genre due to the ability to just freely 
            build whatever I want and do the things I want to do. I especially love older games just because of the simplicity of the art, the graphics, 
            the gameplay, and the overall feel of the games. They were made during the times where computers were very simple and they couldn't process 
            as much as they can now.</p>
    </div>
</body>
</html>