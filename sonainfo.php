<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kyan Woods - Fursona Info</title>
        <meta name="description" content="This is my personal website." />
        <link rel="stylesheet" href="style.css" /> 
        <a href="about.php">About Me</a>
        <a href="gamedatabase.php">List of Games Submitted</a>
        <a href="games.php">Favorite Games</a>
        <a href="index.php">Game Submission</a>
        <a href="info.php">More Information</a>
        <a href="sonainfo.php">Info on my Fursonas</a>
    </head>

<body>
    <div class="divclass">
        <h1>Fursona Info</h1>
        <p class="greenText">I have a lot of fursonas that I have made within just a single year of considering myself to be a furry. 
            Some made by me and some made based on ideas from other sources. This page will contain probably 2-3 of my sonas that I have 
            information on. Most sonas of mine just have vague descriptions of appearances and very very short and basic descriptions of their personalities 
            and whatnot.</p><br>
        
        <h3>Project WarpZone</h3>

        <img src="https://mail.google.com/mail/u/0?ui=2&ik=4397a5a89c&attid=0.2&permmsgid=msg-f:1747877923062275331&th=1841b5b5dadfdd03&view=att&disp=safe" 
        alt="Protogen with multiple danger symbols" width="300" height="350">
        <p class="orangeText">This is my main fursona named Project WarpZone. It is mainly shortened to Warp or just WarpZone since the first part, 
            "Project", is not exactly his name. The first part is mainly due to his lore which is just how he was created in a lab and 
            was supposed to be an organic type of transport method. Warp is a protogen who is mainly trying to keep to himself and stay 
            away from others due to his distrust created via the numerous tests and trials and the many terrible things done to 
            him during the time before he escaped the lab he was created in.</p>
    </div>
</body>
</html>