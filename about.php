<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kyan Woods - About Me</title>
        <meta name="description" content="This is my personal website." />
        <link rel="stylesheet" href="style.css" /> 
        <a href="about.php">About Me</a>
        <a href="gamedatabase.php">List of Games Submitted</a>
        <a href="games.php">Favorite Games</a>
        <a href="index.php">Game Submission</a>
        <a href="info.php">More Information</a>
        <a href="sonainfo.php">Info on my Fursonas</a>
    </head>

<body>
    <div class="divclass">
        <h1>About me</h1>
        <p class="greenText">Heya. This is my website. I am a proud furry. I enjoy building things with legos and 
            I LOVE building small little robots and stuff. I enjoy coding things as well like websites with Visual Studio Code and some 
            text-based games with Visual Studio.</p>
        <p class="greenText">I am 16 years old and I am currently in 10th grade in high school as of writing this code. I plan on doing some job in 
            the future that deals with robotics or jobs that require use of CNC machinery and possibly welding at some point. As of writing this line of code, I 
            work at Cedar Point in Sandusky as a Food Service Associate in Perkins Restaurant and Bakery in Hotel Breakers in 
            the Cedar Point Resort area.</p>
        <p class="greenText">I have Attention-Deficit/Hyperactivity Disorder, ADHD for short. According to <a href="https://www.verywellmind.com/adhd-overview-4581801">this 
            webpage article,</a> "Attention-deficit/hyperactivity disorder (ADHD) is a 
            common neurobehavioral condition that is usually first diagnosed during childhood. More than six million children between the ages of two and 
            17 have been diagnosed with ADHD. It is characterized by patterns of inattention, hyperactivity, and impulsivity that make it difficult for 
            people to pay attention and control their behaviors. ADHD is a lifelong condition. While symptoms do change with time, they can 
            still interfere with an adult's functioning. Relationships, health, work, and finances are just a few areas that may be impacted. 
            There are treatment options, including medications and therapies, as well as coping strategies that can help you to live well with ADHD."</p>
        <p class="greenText">ADHD is a mental health disorder in which many people are affected. There is no cure for it. There is no way to completely get over it. There is 
            medicine that is prescribed for peeople with ADHD but it only helps them control or limit the symptoms of the disorder. 
            "Attention-deficit/hyperactivity disorder (ADHD) is a neurodevelopmental condition that is characterized by symptoms of hyperactivity, 
            impulsivity, and inattention. Unfortunately, adults and children with ADHD are often labeled as unmotivated, lazy, or even apathetic. 
            These negative labels are unfair and hurtful. Instead of simple laziness or a lack of motivation, this 'immobility' or 'sluggishness' 
            often reflects the impairments in executive function that can be associated with ADHD. Understanding these impairments is important in order to 
            correct common misperceptions about ADHD." According to <a href="https://www.verywellmind.com/adhd-and-motivation-20470">this article,</a> 
            "While people who have ADHD are often good at making quick decisions in the moment, they may struggle 
            when they are working on tasks that require organizing lots of information. They may feel like they are bogged down with too much information, 
            which can feel overwhelming. Figuring out how and where to begin may seem impossible. This sense of paralysis can quickly lead to feelings of 
            being overwhelmed, procrastination, and avoidance, and ultimately results in problems with productivity."</p>
        <p class="greenText"> In the same article later on, "Even if the person is able to begin the task, they may have great difficulty staying alert and persisting in this effort. 
            Though they may know what they need to do to get things completed, as hard as they try, they just can't. Boredom results in all 
            sorts of problems for kids and adults with ADHD. Maintaining focus on a boring task may seem nearly impossible as their attention wanders away to 
            more interesting activities and thoughts. What can also happen is that after repeated frustrations, the child or adult with ADHD can begin to feel 
            less motivated. It can be hard to get excited and hopeful about something and then crash down again and again. Because ADHD causes problems with 
            starting, organizing, and sticking with tasks, people often end up feeling bored or frustrated. Eventually, these patterns begin to affect 
            motivation levels as well."</p>
    </div>
</body>
</html>