
# Kyan Woods Final Project

A simple website that includes 6 pages. One for submitting games, one for viewing submitted games, and 4 other pages that have information on them. A few pages have links to go to outside sources for information I gathered from said sources and a few pages have images for things I thought needed visual depictions of.


## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://docs.google.com/presentation/d/1wGTwq7cYszEElpjKxpLi4kl0K4j-RgJEauHb9FLn8Jc/edit?usp=sharing)

